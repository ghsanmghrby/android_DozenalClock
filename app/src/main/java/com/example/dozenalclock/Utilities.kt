/*
 * Copyright (C) 2019 Donald Goodman.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.example.dozenalclock

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@RequiresApi(Build.VERSION_CODES.O)
fun get_doztime(): String {
    val currtime = LocalDateTime.now()
    val hrf = DateTimeFormatter.ofPattern("HH")
    val minf = DateTimeFormatter.ofPattern("mm")
    val secf = DateTimeFormatter.ofPattern("ss")
    val hr = currtime.format(hrf)
    val min = currtime.format(minf)
    val sec = currtime.format(secf)
    val biqtims = ((min.toInt() * 60) + sec.toInt()) / 0.17361111111
    var dhr = hr.toInt().toString(12)
    var dbiqtims = biqtims.toInt().toString(12).padStart(4,'0')
    dhr = dhr.replace("a","X")
    dhr = dhr.replace("b","E")
    dbiqtims = dbiqtims.replace("a","X")
    dbiqtims = dbiqtims.replace("b","E")
    return "$dhr;$dbiqtims"
}
@RequiresApi(Build.VERSION_CODES.O)
fun get_shorttime(): String {
    val currtime = LocalDateTime.now()
    val hrf = DateTimeFormatter.ofPattern("HH")
    val minf = DateTimeFormatter.ofPattern("mm")
    val secf = DateTimeFormatter.ofPattern("ss")
    val hr = currtime.format(hrf)
    val min = currtime.format(minf)
    val sec = currtime.format(secf)
    val biqtims = ((min.toInt() * 60) + sec.toInt()) / 25
    var dhr = hr.toInt().toString(12).padStart(2,'0')
    var dbiqtims = biqtims.toString(12).padStart(2,'0')
    dhr = dhr.replace("a","X")
    dhr = dhr.replace("b","E")
    dbiqtims = dbiqtims.replace("a","X")
    dbiqtims = dbiqtims.replace("b","E")
    return "$dhr;$dbiqtims"
}
@RequiresApi(Build.VERSION_CODES.O)
fun get_dozdate(): String {
	val currdate = LocalDateTime.now()
	val df = DateTimeFormatter.ofPattern("dd")
	val dm = DateTimeFormatter.ofPattern("LLL")
	val dy = DateTimeFormatter.ofPattern("yyyy")
	val day = currdate.format(df)
	val year = currdate.format(dy)
    val mon = currdate.format(dm)
	var dday = day.toInt().toString(12)
    dday = dday.replace("a","X")
    dday = dday.replace("b","E")
	var dyear = year.toInt().toString(12)
    dyear = dyear.replace("a","X")
    dyear = dyear.replace("b","E")
	return "$dday $mon $dyear"
}
