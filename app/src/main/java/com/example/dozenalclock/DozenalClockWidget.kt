/*
 * Copyright (C) 2019 Donald Goodman.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.example.dozenalclock

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.os.Build
import android.widget.RemoteViews
import androidx.annotation.RequiresApi
import android.app.PendingIntent
import android.content.Intent
import android.content.ComponentName

/**
 * Implementation of App Widget functionality.
 */
class DozenalClockWidget : AppWidgetProvider() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
        val widgetText = get_shorttime()
        val dateText = get_dozdate()
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    companion object {

        @RequiresApi(Build.VERSION_CODES.O)
        internal fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {
            val widgetText = get_shorttime()
            val dateText = get_dozdate()
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.dozenal_clock_widget)
            views.setTextViewText(R.id.appwidget_text, widgetText)
            views.setTextViewText(R.id.widget_dateview, dateText)
            val labelIntent = get_proofed_Intent(context)
            val labelPendingIntent = PendingIntent.getBroadcast(
                context,
                appWidgetId,
                labelIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            views.setOnClickPendingIntent(R.id.appwidget_text, labelPendingIntent)
            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
        /**
         * Utility method to ensure that when we want an Intent that fire ACTION_APPWIDGET_UPDATE, the extras are correct.<br></br>
         * The default implementation of onReceive() will discard it if we don't add the ids of all the instances.
         * @param context
         * @return; from Malubu, licensed Apache 2.0
         */
        protected fun get_proofed_Intent(context: Context): Intent {
            val appWidgetManager = AppWidgetManager.getInstance(context)
            val thisAppWidget = ComponentName(
                context.packageName,
                DozenalClockWidget::class.java!!.getName()
            )
            val appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget)
            val intent = Intent(context, DozenalClockWidget::class.java)
            intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds)
            return intent
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
    }
}
